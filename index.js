const { handleBrewBox } = require("./logic/brewLogic");
const {uploadStateRecord, checkBrewConfig, getBrewConfig} = require("./services/brewService");
const { Relay } = require("./domain/models/components/relay");
const { DHT } = require("./domain/models/components/DHT");
const { DS18B20 } = require("./domain/models/components/DS18B20");
const { BrewBox } = require("./domain/models/brewBox");
// physical components setup
const heater = new Relay(24, true, 220, "Heating blanket in Beer-Box");
const cooler = new Relay(25, true, 220, "Secondary power outlet on Beer-Box");
const heater2 = new Relay(17, true, 220, "Heating blanket in Beer-Box 2");
const cooler2 = new Relay(18, true, 220, "Secondary power outlet on Beer-Box 2");

heater.off();
heater2.off();
cooler.off();
cooler2.off();

// const beerThermometer = new DS18B20('28-00ff98430c24', "DS18B20 in Beer");
const beerThermometer = new DS18B20(false, "DS18B20 in Beer");
const beerThermometer2 = new DS18B20(false, "DS18B20 in Beer");
// const airThermometer = new DHT(23, "DHT11 out of the Beer box");

const BrewHandler = BrewBox => {
  checkBrewConfig(BrewBox).then(config => {
    if (config) {
      console.log(config);
      BrewBox.updateConfig(config);
    }
  });
  handleBrewBox(BrewBox).then(currentState => {
    uploadStateRecord(currentState);
  });
};

// ----------- MAIN PROGRAM RUN -----------
console.log("Brew-Master started. Let the brew begin!");

getBrewConfig().then(brewConfig => {
  const brewBox = new BrewBox('001', brewConfig, heater, beerThermometer);
  const brewBox2 = new BrewBox('002', brewConfig, heater2, beerThermometer2);
  // Triggerirng the main loop of the program
  setInterval(() => BrewHandler(brewBox), 60 * 1000);
  setInterval(() => BrewHandler(brewBox2), 60 * 1000);
});
