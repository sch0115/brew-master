const mongo = require("../util/database");
const {APP_SETTINGS} = require('../app.settings');


module.exports.getBrewConfigFromDb = async () => {
  let config;
  const db = await mongo.getDb();
  try {
    const configQueryResults = await db
      .collection(APP_SETTINGS.collection)
      .find({ entityType: "brewConfig" })
      .sort({_id:-1})
      .limit(1).toArray();
    config = configQueryResults[0];
  } catch (e) {
    console.log("Brew config was not loaded! ", e);
  }
  return config;

};

module.exports.uploadStateRecord = async state => {
  const db = await mongo.getDb();
  const stateRecord = {
    entityType: "stateRecord",
    ...state
  };

  if (db) {
    db.collection(APP_SETTINGS.collection)
      .insertOne(stateRecord)
      .then(() => {
        console.log("inserted ", stateRecord, " into DB");
      })
      .catch(err => console.log(err));
  } else {
    console.error("State record was not inserted");
  }
};
