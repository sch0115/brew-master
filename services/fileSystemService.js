const fs = require("fs");

module.exports.updateConfigFile = (newCongif) => {
  fs.writeFile("brewConfig.json", JSON.stringify(newCongif), "utf8", err => {
    if (err) {
      console.log("An error occured while writing Config file .");
      console.log(err);
    } else {
      console.log("Config file has been updated.");
    }
  });
};

module.exports.getBrewConfigFromFile = () => {
  if (fs.existsSync("brewConfig.json")) {
    const rawdata = fs.readFileSync("brewConfig.json");
    return JSON.parse(rawdata);
  }
};