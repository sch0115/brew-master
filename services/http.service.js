const axios = require('axios');
console.log('axios', axios);


export default {
  /**
   * HTTP Get
   * @param url {string}
   * @param options {object}
   * @return {Promise.<T>|*}
   */
  get(url, options = {}) {
    return axios.get(url, options);
  },
  /**
   * HTTP Post
   * @param url {string}
   * @param body {object}
   * @param options {object}
   * @return {Promise.<T>|*}
   */
  post(url, body, options = {}) {
    return axios.post(url, body, options);
  }
};
