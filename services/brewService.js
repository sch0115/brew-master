const DatabaseService = require("./databaseService");
const FileSystemService  = require("./fileSystemService");


module.exports.checkBrewConfig = async (brewBox) => {
  let configFromDb = await DatabaseService.getBrewConfigFromDb();
  if (configFromDb && brewBox.brewConfig._id.toString() !== configFromDb._id.toString()) {
    FileSystemService.updateConfigFile(configFromDb);
    return configFromDb;
  }
};

module.exports.getBrewConfig = async () => {
  let configFromDb = await DatabaseService.getBrewConfigFromDb();
  return configFromDb || FileSystemService.getBrewConfigFromFile();
};

module.exports.uploadStateRecord = async state => {
  await DatabaseService.uploadStateRecord(state);
};
