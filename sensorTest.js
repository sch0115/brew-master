const { Relay } = require("./domain/models/components/relay");
const { DS18B20 } = require("./domain/models/components/DS18B20");
// const { DHT } = require("./domain/models/components/DHT");

const sensor = require('ds18b20-raspi');


const relay17 = new Relay(17, true, 220, "Test Relay on pin 17");
// const relay18 = new Relay(18, true, 220, "Test Relay on pin 18");
const relay21 = new Relay(24, true, 220, "Test Relay on pin 24");
const relay23 = new Relay(23, true, 220, "Test Relay on pin 23");
const ds18b20 = new DS18B20('28-00ff98430c24', "DS18B20 in Test");

// const dht11 = new DHT(12);

setInterval(async () => {
  relay17.isOn() ? relay17.off() : relay17.on();
  // relay18.isOn() ? relay18.off() : relay18.on();
  relay23.isOn() ? relay23.off() : relay23.on();
  relay21.isOn() ? relay21.off() : relay21.on();
  console.log("Test DS18B20 is measuring: ", ds18b20.getTemperature());
  console.log("Test DHT11 is measuring: ", await dht11.getTemperature(), " C");
  console.log("Test DHT11 is measuring: ", await dht11.getHumidity(), "%");
}, 3000);


const list = sensor.list();
console.log(list);

// async version
sensor.list((err, deviceIds) => {
  if (err) {
    console.log(err);
  } else {
    console.log('One-wire devices list: ');
    console.log(deviceIds);
  }
});

var rpiDhtSensor = require('rpi-dht-sensor');

var dht = new rpiDhtSensor.DHT11(12);

function read () {
  var readout = dht.read();

  console.log('Temperature: ' + readout.temperature.toFixed(2) + 'C, ' +
    'humidity: ' + readout.humidity.toFixed(2) + '%');
  setTimeout(read, 5000);
}
read();