async function handleBrewBox(BrewBox) {
  console.log("Handling the environment in regular loop cycle");
  const currentState = await BrewBox.updateAll();
  //Handle temperature
  if (
    currentState.beerTemperature > currentState.targetBeerTemperature &&
    currentState.heatingOn
  ) {
    console.log("heating => OFF");
    BrewBox.turnHeatingOff();
  } else if (
    currentState.beerTemperature < currentState.targetBeerTemperature &&
    !currentState.heatingOn
  ) {
    console.log("heating => ON");
    BrewBox.turnHeatingOn();
  }

  return BrewBox.getState();
}

module.exports = {
  handleBrewBox
};
