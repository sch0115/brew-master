const {MongoClient} = require("mongodb");

let _db;

const mongoConnect = async () => {
  try {
    const client = await MongoClient.connect(
      "mongodb+srv://sch0115:Passw0rd01@cluster0-sih4m.mongodb.net/test?retryWrites=true",
      { useNewUrlParser: true }
    );
    _db = client.db();
    console.log("Database connected!");
  } catch (e) {
    console.log(e);
  }

  return _db;
};

exports.getDb = async () => {
  return _db || (await mongoConnect());
};
