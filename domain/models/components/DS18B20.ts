// @ts-ignore
import * as sensor from 'ds18b20-raspi';


export class DS18B20 {
    private id: any;
    private connection: any;
    private description: any;

    constructor(id, description) {
        this.id = id;
        this.description = description;
        this.connection = sensor;
    }

    getTemperature() {
		if (this.id) {
			return this.connection.readC(this.id);
		}
        return this.connection.readSimpleC();
    }

    getDescription() {
        return this.description;
    }

    getState() {
        return 'ds18b20 with id ' + this.id + ' reads ' + this.getTemperature() + 'C ';
    }

    destroy() {
        console.log('Destroy of DS18D20 - Not Implemented Yet');
    }
};
