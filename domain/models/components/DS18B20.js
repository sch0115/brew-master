"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const sensor = require("ds18b20-raspi");
class DS18B20 {
    constructor(id, description) {
        this.id = id;
        this.description = description;
        this.connection = sensor;
    }
    getTemperature() {
        if (this.id) {
            return this.connection.readC(this.id);
        }
        return this.connection.readSimpleC();
    }
    getDescription() {
        return this.description;
    }
    getState() {
        return 'ds18b20 with id ' + this.id + ' reads ' + this.getTemperature() + 'C ';
    }
    destroy() {
        console.log('Destroy of DS18D20 - Not Implemented Yet');
    }
}
exports.DS18B20 = DS18B20;
;
//# sourceMappingURL=DS18B20.js.map