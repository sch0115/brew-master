"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const rpi_dht_sensor_1 = require("rpi-dht-sensor");
class DHT {
    constructor(Gpio, description) {
        this.Gpio = Gpio;
        this.description = description;
        this.readout = null;
        this.lastUpdate = null;
        this.connection = new rpi_dht_sensor_1.DHT11(2);
    }
    getTemperature() {
        return __awaiter(this, void 0, void 0, function* () {
            const readout = yield this._read();
            return readout.temperature.toFixed(2);
        });
    }
    getHumidity() {
        return __awaiter(this, void 0, void 0, function* () {
            const readout = yield this._read();
            return readout.humidity.toFixed(2);
        });
    }
    getDescription() {
        return this.description;
    }
    getState() {
        return 'DHT11 on port ' + this.Gpio + ' reads ' + this.getTemperature() + 'C ' + this.getHumidity() + '% humidity';
    }
    destroy() {
        console.log('Destroy of DHT - Not Implemented Yet');
    }
    _read() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.lastUpdate < Date.now() - 60) {
                this.readout = yield this.connection.read();
            }
            return this.readout;
        });
    }
}
exports.DHT = DHT;
;
//# sourceMappingURL=DHT.js.map