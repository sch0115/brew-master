"use strict";
// ===================================================================
// RELAY module
// Parameters: GPIO, isClosed, current
//Methods: On, Off, isOn, getState
// var Gpio = require('onoff').Gpio;
// @ts-ignore
Object.defineProperty(exports, "__esModule", { value: true });
const onoff_1 = require("onoff");
class Relay {
    constructor(gpio, isClosed, current, description) {
        this.gpio = gpio;
        this.current = current;
        this.closed = isClosed;
        this.description = description;
        this.onSignal = this.closed ? 1 : 0;
        this.offSignal = this.closed ? 0 : 1;
        this.connection = new onoff_1.Gpio(gpio, 'out');
    }
    isOn() {
        return this.connection.readSync() === this.onSignal;
    }
    on() {
        this.connection.writeSync(this.onSignal);
    }
    off() {
        this.connection.writeSync(!this.onSignal);
    }
    getState() {
        return 'RELAY on port ' + this.gpio + ' is ' + this.isOn() ? 'ON' : 'OFF';
    }
    getDescription() {
        return this.description;
    }
    destroy() {
        this.connection.unexport();
    }
}
exports.Relay = Relay;
;
//# sourceMappingURL=relay.js.map