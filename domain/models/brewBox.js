"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { DHT } from "./components/DHT";
const TARGET_TEMPERATURE = 21;
const DIALATION = 0;
const COMPONENT_NAME = "BrewBox";
class BrewBox {
    // TODO change to object and arrays of components
    constructor(brewBoxId, brewConfig, heater, 
    // airThermometer: DHT,
    beerThermometer) {
        this.component = COMPONENT_NAME;
        this.brewBoxId = brewBoxId;
        this.heater = heater;
        this.beerThermometer = beerThermometer;
        // this.airThermometer = airThermometer || {};
        this.heating = this.heater.isOn();
        this.brewConfig = brewConfig;
        this.targetBeerTemperature =
            brewConfig.targetBeerTemperature || TARGET_TEMPERATURE;
        this.dilation = brewConfig.dilation || DIALATION;
    }
    updateConfig(config) {
        console.log("Updating brewConfig to " + config);
        this.brewConfig = config;
        this.setTargetTemperature(config.targetBeerTemperature);
        this.dilation = config.dilation || this.dilation;
    }
    isHeating() {
        return this.heater.isOn();
    }
    // TODO implement array of heaters
    turnHeatingOn() {
        this.heater.on();
    }
    turnHeatingOff() {
        this.heater.off();
    }
    setTargetTemperature(targetTemperature) {
        this.targetBeerTemperature = targetTemperature;
    }
    getTemperature() {
        return this.beerTemperature;
    }
    updateAll() {
        return __awaiter(this, void 0, void 0, function* () {
            this.beerTemperature = this.beerThermometer.getTemperature();
            this.heatingOn = this.isHeating();
            // this.airTemperature = await this.airThermometer.getTemperature();
            // this.airHumidity = await this.airThermometer.getHumidity();
            return this.getState();
        });
    }
    getState() {
        return {
            brewBoxId: this.brewBoxId,
            component: this.component,
            timestamp: new Date(),
            beerTemperature: this.beerTemperature,
            targetBeerTemperature: this.targetBeerTemperature,
            heatingOn: this.heatingOn,
            // airTemperature: this.airTemperature,
            // airHumidity: this.airHumidity,
            brewConfig: this.brewConfig
        };
    }
}
exports.BrewBox = BrewBox;
//# sourceMappingURL=brewBox.js.map