import { Relay } from "./components/relay";
import { DS18B20 } from "./components/DS18B20";
// import { DHT } from "./components/DHT";

const TARGET_TEMPERATURE = 21;
const DIALATION = 0;

const COMPONENT_NAME = "BrewBox";
export class BrewBox {
  private component = COMPONENT_NAME;

  private heater: Relay;
  private beerThermometer: DS18B20;
  private beerTemperature: string;
  private heatingOn: boolean;
  // private airThermometer: any;
  // private airTemperature: number;
  // private airHumidity: number;

  // CONFIG VALUES
  private targetBeerTemperature: number;
  private dilation: number;
  private heating: boolean;
  private brewConfig: any;
  private brewBoxId: string;

  // TODO change to object and arrays of components
  constructor(
    brewBoxId: string,
    brewConfig: any,
    heater: Relay,
    // airThermometer: DHT,
    beerThermometer: DS18B20
  ) {
    this.brewBoxId = brewBoxId;
    this.heater = heater;
    this.beerThermometer = beerThermometer;
    // this.airThermometer = airThermometer || {};
    this.heating = this.heater.isOn();
    this.brewConfig = brewConfig;
    this.targetBeerTemperature =
      brewConfig.targetBeerTemperature || TARGET_TEMPERATURE;
    this.dilation = brewConfig.dilation || DIALATION;
  }

  updateConfig(config) {
    console.log("Updating brewConfig to " + config);
    this.brewConfig = config;
    this.setTargetTemperature(config.targetBeerTemperature);
    this.dilation = config.dilation || this.dilation;
  }

  isHeating() {
    return this.heater.isOn();
  }

  // TODO implement array of heaters
  turnHeatingOn() {
    this.heater.on();
  }
  turnHeatingOff() {
    this.heater.off();
  }

  setTargetTemperature(targetTemperature) {
    this.targetBeerTemperature = targetTemperature;
  }

  getTemperature() {
    return this.beerTemperature;
  }

  async updateAll() {
    this.beerTemperature = this.beerThermometer.getTemperature();
    this.heatingOn = this.isHeating();
    // this.airTemperature = await this.airThermometer.getTemperature();
    // this.airHumidity = await this.airThermometer.getHumidity();
    return this.getState();
  }

  getState() {
    return {
      brewBoxId: this.brewBoxId,
      component: this.component,
      timestamp: new Date(),
      beerTemperature: this.beerTemperature,
      targetBeerTemperature: this.targetBeerTemperature,
      heatingOn: this.heatingOn,
      // airTemperature: this.airTemperature,
      // airHumidity: this.airHumidity,
      brewConfig: this.brewConfig
    };
  }
}
